
BCM prototypes
====

3 prototypes

1. ColorDetection - prototype to test out color detection 
2. ShapeDetection - prototype to test out shape detection. 
3. ArUco / Marker detection - prototype to test out marker detection. 
4. last one is basic test of Orbbec SDK


note that things are mostly tested with still images for now.