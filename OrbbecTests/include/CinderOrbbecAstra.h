#pragma once

#include "cinder/Vector.h"
#include "astra/astra.hpp"
#include "cinder/gl/Texture.h"
#include "cinder/Surface.h"
#include "cinder/Channel.h"
#include "astra_core\Frame.hpp"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
class CinderOrbbecAstra : public astra::FrameListener {

	astra::StreamSet streamset;
	astra::StreamReader reader;

	ci::Surface8u mColordata;
	ci::gl::TextureRef mColorTexture;

	int width;
	int height;
	bool bSetup;

	virtual void on_frame_ready(astra::StreamReader& reader,
		astra::Frame& frame) override;
public:
	CinderOrbbecAstra();
	//! clean up Camera
	void kill();

	//! Sets camera up 
	void setup();

	//! updates camera info
	void update();

	//! initializes color stream
	void initColorStream();

	//! debugging function to render color frame
	void drawColorFrame();
};