#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"
#include "astra/astra.hpp"
#include "CinderOrbbecAstra.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class OrbbecTestsApp : public App {
  public:
	  ~OrbbecTestsApp();

	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;


	CinderOrbbecAstra astra;

};

OrbbecTestsApp::~OrbbecTestsApp() {
	astra.kill();
}

void OrbbecTestsApp::setup()
{

	astra.setup();
	astra.initColorStream();

	
	

}

void OrbbecTestsApp::mouseDown( MouseEvent event )
{
}

void OrbbecTestsApp::update()
{

	astra.update();

}

void OrbbecTestsApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
	astra.drawColorFrame();
}

CINDER_APP( OrbbecTestsApp, RendererGl )
