#include "CinderOrbbecAstra.h"
#include "cinder/Log.h"
#include "astra_core/astra_core.hpp"
using namespace ci;
using namespace std;

CinderOrbbecAstra::CinderOrbbecAstra():
width(640),
height(480){
	
}

void CinderOrbbecAstra::setup() {
	// initialize Astra stuff
	astra::initialize();

	// general texture settings
	gl::Texture::Format fmt;
	fmt.setInternalFormat(GL_RGBA32F);
	fmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	fmt.minFilter(GL_NEAREST);
	fmt.magFilter(GL_NEAREST);

	//streamset = astra::StreamSet(uri.c_str());
	reader = astra::StreamReader(streamset.create_reader());

	// make sure reader knows to publish info to this class.
	reader.add_listener(*this);

	// change setup flag so we can make sure user has called this function prior to other functions.
	bSetup = true;

	// ================ SETUP COLOR STREAM NECESSITIES =================== //
	mColordata = Surface8u(width, height, false);
	mColorTexture = gl::Texture::create(mColordata,fmt);
}

void CinderOrbbecAstra::on_frame_ready(astra::StreamReader& reader,
	astra::Frame& frame){

	
	// update color frame data if valid.
	auto colorFrame = frame.get<astra::ColorFrame>();
	if (colorFrame.is_valid()) {
		colorFrame.copy_to((astra::RgbPixel*) mColordata.getData());
		mColorTexture->update(mColordata);
	}
	
}
void CinderOrbbecAstra::kill() {

	// make sure Astra terminates properly
	astra::terminate();
}

void CinderOrbbecAstra::update() {

	if (!bSetup) {
		return;
	}

	// tell camera to update itself.
	astra_update();
}
void CinderOrbbecAstra::initColorStream() {
	if (!bSetup) {
		return;
	}

	astra::ImageStreamMode colorMode;
	auto colorStream = reader.stream<astra::ColorStream>();

	colorMode.set_width(width);
	colorMode.set_height(height);
	colorMode.set_pixel_format(astra_pixel_formats::ASTRA_PIXEL_FORMAT_RGB888);
	colorMode.set_fps(30);

	colorStream.set_mode(colorMode);
	colorStream.start();
}

void CinderOrbbecAstra::drawColorFrame() {
	gl::draw(mColorTexture, Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
}