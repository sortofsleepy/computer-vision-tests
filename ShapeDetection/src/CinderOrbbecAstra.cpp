#include "CinderOrbbecAstra.h"
#include "cinder/Log.h"
#include "astra_core/astra_core.hpp"
using namespace ci;
using namespace std;

CinderOrbbecAstra::CinderOrbbecAstra() :
	width(640),
	height(480),
	mIsColorFrameNew(false) {

}

void CinderOrbbecAstra::setup() {
	// initialize Astra stuff
	astra::initialize();

	// general texture settings
	gl::Texture::Format fmt, depthFmt, irFmt;
	fmt.setInternalFormat(GL_RGBA32F);
	fmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	fmt.minFilter(GL_NEAREST);
	fmt.magFilter(GL_NEAREST);

	depthFmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	depthFmt.minFilter(GL_NEAREST);
	depthFmt.magFilter(GL_NEAREST);

	irFmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	irFmt.minFilter(GL_NEAREST);
	irFmt.magFilter(GL_NEAREST);

	//streamset = astra::StreamSet(uri.c_str());
	reader = astra::StreamReader(streamset.create_reader());

	// make sure reader knows to publish info to this class.
	reader.add_listener(*this);

	// change setup flag so we can make sure user has called this function prior to other functions.
	bSetup = true;

	// ================ SETUP COLOR STREAM NECESSITIES =================== //
	mColordata = Surface8u(width, height, false);
	mColorTexture = gl::Texture::create(mColordata, fmt);

	// setup depth data
	mDepthData = Channel16u(width, height);
	mDepthTexture = gl::Texture::create(mDepthData, depthFmt);

	// setup ir data
	mInfaredData = Surface8u(width, height, true, SurfaceChannelOrder::RGB);
	mInfaredTexture = gl::Texture::create(mInfaredData, irFmt);
}

void CinderOrbbecAstra::on_frame_ready(astra::StreamReader& reader,
	astra::Frame& frame) {


	// update color frame data if valid.
	auto colorFrame = frame.get<astra::ColorFrame>();
	if (colorFrame.is_valid()) {
		mIsColorFrameNew = true;
		colorFrame.copy_to((astra::RgbPixel*) mColordata.getData());
		mColorTexture->update(mColordata);
	}
	else {
		mIsColorFrameNew = false;
	}

	// update depth data if valid.
	auto depthFrame = frame.get<astra::DepthFrame>();
	if (depthFrame.is_valid()) {


		depthFrame.copy_to((int16_t*)mDepthData.getData());

		//TODO maybe re-color depth info, comes through ok, though.
		mDepthTexture->update(mDepthData);

	}

	// update ir data if valid
	auto irFrame = frame.get<astra::InfraredFrameRgb>();
	if (irFrame.is_valid()) {

		irFrame.copy_to((astra::RgbPixel*)mInfaredData.getData());
		mInfaredTexture->update(mInfaredData);
	}

}
void CinderOrbbecAstra::kill() {

	// make sure Astra terminates properly
	astra::terminate();
}

void CinderOrbbecAstra::update() {

	if (!bSetup) {
		return;
	}

	// tell camera to update itself.
	astra_update();
}
void CinderOrbbecAstra::initColorStream() {
	if (!bSetup) {
		return;
	}

	astra::ImageStreamMode colorMode;
	auto colorStream = reader.stream<astra::ColorStream>();

	colorMode.set_width(width);
	colorMode.set_height(height);
	colorMode.set_pixel_format(astra_pixel_formats::ASTRA_PIXEL_FORMAT_RGB888);
	colorMode.set_fps(30);

	colorStream.set_mode(colorMode);
	colorStream.start();
}

void CinderOrbbecAstra::initDepthStream() {
	if (!bSetup) {
		return;
	}

	astra::ImageStreamMode depthMode;
	auto depthStream = reader.stream<astra::DepthStream>();

	depthMode.set_width(width);
	depthMode.set_height(height);
	depthMode.set_pixel_format(astra_pixel_formats::ASTRA_PIXEL_FORMAT_DEPTH_MM);
	depthMode.set_fps(30);



	depthStream.set_mode(depthMode);
	depthStream.start();
}

void CinderOrbbecAstra::initIRStream() {
	if (!bSetup) {
		return;
	}

	astra::ImageStreamMode IRMode;
	auto irStream = reader.stream<astra::InfraredStream>();

	IRMode.set_width(width);
	IRMode.set_height(height);
	IRMode.set_pixel_format(astra_pixel_formats::ASTRA_PIXEL_FORMAT_RGB888);
	IRMode.set_fps(30);

	irStream.set_mode(IRMode);
	irStream.start();
}

ci::gl::TextureRef CinderOrbbecAstra::getColorTexture() {
	if (mIsColorFrameNew) {
		return mColorTexture;
	}
}

ci::Surface8u CinderOrbbecAstra::getColorData() {
	if (mIsColorFrameNew) {
		return mColordata;
	}
}

void CinderOrbbecAstra::drawColorFrame() {
	gl::draw(mColorTexture, Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
}

void CinderOrbbecAstra::drawDepthFrame() {
	gl::draw(mDepthTexture, Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
}

void CinderOrbbecAstra::drawIRFrame() {
	gl::draw(mInfaredTexture, Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
}