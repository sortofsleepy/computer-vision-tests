#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"
#include "astra/astra.hpp"
#include "CinderOrbbecAstra.h"
#include "CinderOpenCV.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace ci::app;
using namespace std;

typedef struct{
	int id;
}FoundObject;


/*
	Simple Shape detection test. 

	Based off of gist 
	https://github.com/bsdnoobz/opencv-code/blob/master/shape-detect.cpp

	Includes a rudimentary system of object identification and tracking, so as to be able to figure out when to show animations. 

	Current issues :
	- Need to figure out optimal image processing to allow not just for shapes to be recognized, but also be able to 
	figure out the correct number of objects.

*/
class ShapeDetectionApp : public App {
public:
	~ShapeDetectionApp();

	void setup() override;
	void mouseDown(MouseEvent event) override;
	void checkIfTrackingObject();
	
	void showFoundImage();
	void update() override;
	void draw() override;
	void buildDebug();
	void drawDebug();

	CinderOrbbecAstra astra;

	// all of the objects being tracked
	vector<FoundObject> trackedObjects;

	// valid ids we'd tie to animations
	vector<int> validIds = {
		0,1,2,3,4
	};

	// ids being used.
	vector<int> usedIds;

	//! centroids of all found objects
	vector<cv::Point2f> centroids;

	// ============ DEBUGGING STUFF ================= //

	ci::Surface8u testImage;
	bool debugMode = false;


	ci::gl::TextureRef grayTex;
	ci::Channel grayData;
	

	ci::gl::TextureRef cannyTex;
	ci::Channel cannyData;

	int count = 0;
	int numObjects = 0;
};

ShapeDetectionApp::~ShapeDetectionApp() {
	astra.kill();
}
void ShapeDetectionApp::showFoundImage() {

}
void ShapeDetectionApp::buildDebug() {

	testImage = Surface8u(loadImage(app::loadAsset("triangle3.jpg")));
	//testImage = Surface8u(loadImage(app::loadAsset("square.jpg")));

	gl::Texture::Format fmt;
	fmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	fmt.setMagFilter(GL_NEAREST);
	fmt.setMinFilter(GL_NEAREST);	

	grayData = Channel(640, 480);
	grayTex = gl::Texture::create(grayData, fmt);

	cannyData = Channel(640, 480);
	cannyTex = gl::Texture::create(cannyData, fmt);

	debugMode = true;



}	

void ShapeDetectionApp::setup()
{

	astra.setup();
	astra.initColorStream();

	buildDebug();



}

void ShapeDetectionApp::mouseDown(MouseEvent event)
{
	
}

/**
* Helper function to find a cosine of angle between vectors
* from pt0->pt1 and pt0->pt2
*/
static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1*dx2 + dy1 * dy2) / sqrt((dx1*dx1 + dy1 * dy1)*(dx2*dx2 + dy2 * dy2) + 1e-10);
}


void ShapeDetectionApp::update()
{
	// make sure astra is updating data
	astra.update();

	// if we have an available frame, begin shape detection.
	// Based off of 
	// https://github.com/bsdnoobz/opencv-code/blob/master/shape-detect.cpp

	if (astra.isColorFrameNew()) {

		cv::Mat colorImage(ci::toOcv(testImage));

		// Convert to grayscale
		cv::Mat gray;
		cv::cvtColor(colorImage, gray, CV_BGR2GRAY);

		// Use Canny instead of threshold to catch squares with gradient shading
		// TODO need to tweak settings a bit. Current issue - one triangle doesn't register as a triangle :/ but multiple triangles will 
		// register as triangles and we get the right number of objects.
		cv::Mat bw, thresh;
		cv::GaussianBlur(gray, bw, cv::Size(5, 5), 0);
		cv::Canny(gray, bw, 0, 250, 3);
		cv::threshold(bw, thresh, 60, 255, cv::THRESH_BINARY);

	
		// Find contours
		std::vector<std::vector<cv::Point> > contours;
		cv::findContours(thresh.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

		numObjects = contours.size();

		std::vector<cv::Point> approx;

		if (debugMode) {
			cannyData = Channel(fromOcv(bw));
			cannyTex->update(cannyData);
		}

	
		for (int i = 0; i < contours.size(); i++)
		{
			// Approximate contour with accuracy proportional
			// to the contour perimeter

			//(not sure if there's a difference between next 2 lines)
			cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.04, true);
			//cv::approxPolyDP(contours[i], approx, cv::arcLength(contours[i], true)*0.04, true);
		
			// calculate centroids of contours so we 
			cv::Moments m = cv::moments(contours[i]);
			if (m.m00 != 0) {
				auto centroid = cv::Point2f(m.m10 / m.m00, m.m01 / m.m00);
				centroids.push_back(centroid);
			}
			else {
				auto centroid = cv::Point2f(0, 0);
				centroids.push_back(centroid);
			}



			// looking for triangle shaped objects.
			if (approx.size() == 3)
			{
				// check if we're tracking the object.
				checkIfTrackingObject();
			}

			
		}
	}
}



void ShapeDetectionApp::checkIfTrackingObject() {
	// if we still have some un-used ids and aren't tracking the number of objects in the scene.
	if (usedIds.size() < validIds.size() && trackedObjects.size() != numObjects) {

		// make a new object
		FoundObject obj;

		// do a diff between used and non used ids. 
		vector<int> diff;
		std::set_difference(validIds.begin(), validIds.end(), usedIds.begin(), usedIds.end(), std::inserter(diff, diff.begin()));

		// push new object. 
		// just assigning ids in order but could be random also.
		obj.id = count;
		trackedObjects.push_back(obj);
		usedIds.push_back(count);

		CI_LOG_I("Found a triangle!");
		count++;
	}

}


void ShapeDetectionApp::drawDebug() {
	gl::setMatricesWindow(app::getWindowSize());
	gl::viewport(app::getWindowSize());

	float scaledW = 640.0f / 2.0f;
	float scaledH = 480.0f / 2.0f;

	gl::draw(grayTex, Rectf(0, 0, scaledW, scaledH));


	gl::draw(cannyTex, Rectf(650.0, 0, scaledW, scaledH));


	
}
void ShapeDetectionApp::draw()
{
	gl::clear(Color(0, 0, 0));
	gl::color(Color(1, 0, 0));

	gl::draw(cannyTex);

	// draw centroids
	for (cv::Point2f centroid : centroids) {
		gl::drawSolidCircle(fromOcv(centroid), 10);
	}
	
}

CINDER_APP(ShapeDetectionApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1280, 720);
	settings->setMultiTouchEnabled(false);
})
