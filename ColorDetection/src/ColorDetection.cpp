#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"
#include "astra/astra.hpp"
#include "CinderOrbbecAstra.h"
#include "CinderOpenCV.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"

using namespace ci;
using namespace ci::app;
using namespace std;


/*
Color detection testing - intented for use with IR stream. Code is largely based off of ofxCv

Notes:
- There seems to be a proximity sensor that turns of the IR lasers; this is a guess, but I'm assuming this is the
reason the camera feed kept cutting off.

There is a tool here
https://3dclub.orbbec3d.com/t/universal-download-thread-for-astra-series-cameras/622?source_topic_id=1386 (do a search for proximity)
that is supposed to be able to turn the sensor off, but it is unbuilt, needs OpenNI, and there seems to be a setting treating warnings as errors
which prevents compilation that I can't find  :/

*/

typedef struct {
	
	ci::vec2 position;
	float radius;
}BlobMarker;
class ColorDetectionApp : public App {
public:
	~ColorDetectionApp();

	void setup() override;
	void mouseDown(MouseEvent event) override;
	bool findColorInHSV(cv::Mat inputImage, int maxTargets);

	void update() override;
	void draw() override;

	bool useCapture = true;

	CinderOrbbecAstra astra;

	//! Target color to find
	ci::Color targetColor;

	//! Image to use in testing
	ci::Surface image;

	//! Threshold value when running color test.
	float thresholdValue = 120.0f;

	// stores contours when looking for color.
	vector<vector<cv::Point> > allContours;

	// stores centroids of found points
	vector<cv::Point2f> centroids;

	vector<BlobMarker> blobs;

	gl::TextureRef frame, threshFrame;

	ci::Surface threshSurface;

	cv::Mat thresh;

	cv::Mat gray;
	float mBlobMin = 100;
	float mBlobMax = 5000;

	bool debug = true;
};

ColorDetectionApp::~ColorDetectionApp() {
	if (useCapture) {
		astra.kill();
	}
}

void ColorDetectionApp::setup()
{
	threshFrame = gl::Texture::create(640,480);


	if (!useCapture) {


		targetColor = ci::Color(1, 0, 0);
		image = ci::Surface(loadImage(app::loadAsset("test3.jpg")));
		
		cv::Mat cvImage = toOcv(image);

		cv::Mat gray;
		cv::cvtColor(cvImage, gray, CV_RGB2GRAY);

		cv::Mat blur;
		//cv::GaussianBlur(gray, blur, cv::Size(10,10),3);
		cv::blur(gray, blur, cv::Size(10,10));


		cv::Mat threshold;
		cv::threshold(blur, threshold, 100, 200, CV_THRESH_BINARY);

		threshFrame = gl::Texture::create(fromOcv(threshold));

		findColorInHSV(threshold, 3);
		

	}
	else {
		astra.setup();
		astra.initIRStream();
	}


}


bool ColorDetectionApp::findColorInHSV(cv::Mat inputColor, int maxTargets) {
	// convert iamge to hsv



	// alternative to cv::thresh for color images according to 
	// https://stackoverflow.com/questions/26218280/thresholding-rgb-image-in-opencv
	// also trying out color selection based on ofxCv
	cv::Scalar offset(thresholdValue, thresholdValue, thresholdValue);
	cv::Scalar target(255, 255, 255);
	cv::Scalar lower(100, 100, 100);

	cv::inRange(inputColor, target - offset, target + offset, thresh);

	// clear old data
	allContours.clear();
	blobs.clear();

	cv::findContours(thresh.clone(), allContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	if (debug) {
		threshSurface = Surface(fromOcv(thresh));
		threshFrame->update(threshSurface);
	}

	if (allContours.size() > 0) {

		centroids.clear();
		for (int i = 0; i < allContours.size(); ++i) {
			auto contour = allContours[i];
			
			cv::Point2f center;
			float radius;

			cv::Mat pointsMatrix = cv::Mat(contour);
			cv::minEnclosingCircle(pointsMatrix, center, radius);

		
			if (radius > mBlobMin && radius < mBlobMax) {
				BlobMarker blob;
				blob.position = vec2(center.x, center.y);
				blob.radius = radius;
				blobs.push_back(blob);
				CI_LOG_I("Radius is : " << radius);
			}
			

		}

		return true;
	}
	else {
		return false;
	}

}

void ColorDetectionApp::mouseDown(MouseEvent event)
{
}



void ColorDetectionApp::update()
{

	if (useCapture) {

		// make sure astra is updating data
		astra.update();

		if (astra.isIRFrameNew()) {
			targetColor = ci::Color(1, 1, 1);

			cv::Mat cvImage = toOcv(astra.getIRData());


			// ======== PREPROCESS A BIT TO REMOVE EXTRA NOISE =========== //
			cv::Mat gray;
			cv::cvtColor(cvImage, gray, CV_RGB2GRAY);

			cv::Mat blur;
			//cv::GaussianBlur(gray, blur, cv::Size(10,10),3);
			cv::blur(gray, blur, cv::Size(10, 10));


			cv::Mat threshold;
			cv::threshold(blur, threshold, 100, 200, CV_THRESH_BINARY);

			findColorInHSV(threshold, 3);

		}
	}
}

void ColorDetectionApp::draw()
{
	gl::clear(Color(0, 0, 0));
	gl::color(Color(1, 1, 1));
	if (useCapture) {
		//astra.drawIRFrame();
		gl::draw(threshFrame);

		if (debug) {
			//gl::draw(threshFrame, Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
		}//
	}
	else {
		gl::draw(threshFrame);
	}
	for (BlobMarker marker : blobs) {
		gl::color(ColorA(1, 0, 0,0.5));
		gl::drawSolidCircle(marker.position, marker.radius);
	}
}

CINDER_APP(ColorDetectionApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1280, 720);
	settings->setMultiTouchEnabled(false);
})

