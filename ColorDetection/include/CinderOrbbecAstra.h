#pragma once

#include "cinder/Vector.h"
#include "astra/astra.hpp"
#include "cinder/gl/Texture.h"
#include "cinder/Surface.h"
#include "cinder/Channel.h"
#include "astra_core\Frame.hpp"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"

// This is largely based on 
// https://github.com/mattfelsen/ofxOrbbecAstra

class CinderOrbbecAstra : public astra::FrameListener {

	astra::StreamSet streamset;
	astra::StreamReader reader;

	ci::Surface8u mColordata;
	ci::gl::TextureRef mColorTexture;

	ci::Channel16u mDepthData;
	ci::Channel16u mDepthRenderData;
	ci::gl::TextureRef mDepthTexture;

	//ci::Channel mInfaredData;
	ci::Surface8u mInfaredData;
	ci::gl::TextureRef mInfaredTexture;

	int width;
	int height;
	bool bSetup;

	bool mIsColorFrameNew;
	bool mIsDepthFrameNew;
	bool mIsIRFrameNew;

	virtual void on_frame_ready(astra::StreamReader& reader,
		astra::Frame& frame) override;


public:
	CinderOrbbecAstra();

	ci::gl::TextureRef getColorTexture();

	//! clean up Camera
	void kill();

	//! Sets camera up 
	void setup();

	//! updates camera info
	void update();

	//! initializes color stream
	void initColorStream();

	//! initializes depth stream
	void initDepthStream();

	//! init IR stream
	void initIRStream();


	//! get color data
	ci::Surface8u getColorData();
	ci::Surface8u getIRData();
	ci::Channel16u getDepthdata();

	//! Checks to see if we have a valid and new color frame
	bool isColorFrameNew() {
		return mIsColorFrameNew;
	}

	bool isDepthFrameNew() {
		return mIsDepthFrameNew;
	}

	bool isIRFrameNew() {
		return mIsIRFrameNew;
	}

	//! debugging function to render color frame
	void drawColorFrame();

	void drawDepthFrame();
	void drawIRFrame();
};