#pragma once

#include "opencv2/core.hpp"
#include "CinderOpenCV.h"
#include "cinder/Surface.h"
#include "cinder/Vector.h"
#include "arucolib/markerdetector.h"
#include "arucolib/cameraparameters.h"
#include "CVUtils.h"
#include "cinder/Log.h"
#include <vector>
#include "cinder/gl/gl.h"
using namespace std;
using namespace ci;


// simple struct to define poperties of a marker.
typedef struct {
	ci::vec2 centroid;
	glm::mat3 rotationMatrix;
	ci::vec3 rotationVector;
	std::vector<ci::vec2> mMarkerCorners;
	int id;
	bool rotated;
}ArMarker;


class CinderArUco {
	
	//! Holds reference to current camera matrices.
	cv::Mat cameraMatrices;

	//! size of the marker in meters.
	float markerSizeInMeters;

	//! Marker detector object
	aruco::MarkerDetector mMarkerDetector;

	//! Found markers vector
	std::vector<aruco::Marker> mPreprocessedMarkers;

	//! Stores processed marker data
	std::vector<ArMarker> markers;

public:
	//! Constructor - pass in the marker size in inches and a 3x3 matrix representing the camera parameters.
	//! TODO unsure of how specifically to figure out the right camera matrix but seems if you lop off the last column 
	//! in a 4x4, that'll work. In this case having a blank/identity matrix seems to work well for the purposes
	//! of figuring out rotation.
	CinderArUco(float markerSize = 8.0, cv::Mat cameraMatrices = cv::Mat(3, 3, cv::DataType<float>::type));
	void setMarkerSize(float inches);

	void update(ci::Surface cameraFrame);

	//! Debug method to draw all the corner points of every found marker
	//! Pass in a radius for each corner.
	void draw(int radius = 10);
};