#pragma once

#include "CinderOpenCV.h"
#include "cinder/Matrix44.h"
#include "cinder/Matrix33.h"

using namespace ci;
namespace mocha {

	//! Converts 3x3 cv::Mat to glm counterpart.
	static glm::mat3 convertToGLM(cv::Mat mat,mat3 &inmat) {
		inmat[0][0] = mat.at<float>(0, 0);
		inmat[0][1] = mat.at<float>(0, 1);
		inmat[0][2] = mat.at<float>(0, 2);

		inmat[1][0] = mat.at<float>(1, 0);
		inmat[1][1] = mat.at<float>(1, 1);
		inmat[1][2] = mat.at<float>(1, 2);


		inmat[2][0] = mat.at<float>(2, 0);
		inmat[2][1] = mat.at<float>(2, 1);
		inmat[2][2] = mat.at<float>(2, 2);

		return inmat;
	}

	//! Returns the Euler angles from a 3x3 glm matrix;
	static ci::vec3 getEuler(mat3 &glmmat) {
		glm::quat quat;
		quat = glm::quat_cast(glmmat);
		return glm::eulerAngles(quat);
	}
}