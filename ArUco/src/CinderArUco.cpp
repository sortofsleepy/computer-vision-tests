
#include "CinderArUco.h"

using namespace ci;
using namespace std;


CinderArUco::CinderArUco(float markerSize,cv::Mat cameraMatrices) {
	setMarkerSize(markerSize);
	this->cameraMatrices = cameraMatrices;
}



void CinderArUco::update(ci::Surface cameraFrame) {

	// run marker detector
	mMarkerDetector.detect(toOcv(cameraFrame), mPreprocessedMarkers);


	if (mPreprocessedMarkers.size() > 0) {

		markers.clear();

		for (int i = 0; i < mPreprocessedMarkers.size(); ++i) {
			auto centroid = mPreprocessedMarkers[i].getCenter();


			ArMarker marker;

			// store id 
			marker.id = i;

			marker.centroid = fromOcv(centroid);

			// store corners
			for (int ii = 0; ii < mPreprocessedMarkers[i].size(); ii++) {
				marker.mMarkerCorners.push_back(vec2(mPreprocessedMarkers[i][ii].x, mPreprocessedMarkers[i][ii].y));
			}

			// store rotation - note that the values may not necessarily be correct but
			// easily does the job to allow us to determine when a marker has turned. 
			mPreprocessedMarkers[i].calculateExtrinsics(markerSizeInMeters, cameraMatrices);

			cv::Mat rot;
			cv::Rodrigues(mPreprocessedMarkers[i].Rvec, rot);

			mocha::convertToGLM(rot, marker.rotationMatrix);
			marker.rotationVector = mocha::getEuler(marker.rotationMatrix);

			auto rotation = marker.rotationVector;
			

			// this is roughly holding an object in a "normal", un-rotated orientation, if
			// it deviates from this, then we consider the object to be rotated.
			if (rotation.x > -1 && rotation.x < 2) {
				marker.rotated = false;
			}
			else {
				marker.rotated = true;
			}


			markers.push_back(marker);
		}

	}
}

void CinderArUco::setMarkerSize(float inches) {
	markerSizeInMeters = inches * 0.0254f;
}



void CinderArUco::draw(int radius) {
	gl::ScopedColor color(1, 0, 0);

	for (int i = 0; i < markers.size(); ++i) {
		auto corners = markers[i].mMarkerCorners;
		auto rotated = markers[i].rotated;

		if (rotated) {
			CI_LOG_I("Marker " << i << " is rotated");
		}

		for (vec2 pt : corners) {
			gl::drawSolidCircle(pt, radius);
		}
	}
}