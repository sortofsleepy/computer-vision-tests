#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "opencv2/aruco.hpp"
#include "CinderOpenCV.h"
#include "arucolib/markerdetector.h"
#include "arucolib/cameraparameters.h"
#include "cinder/Capture.h"
#include "cinder/Log.h"
#include "opencv2/aruco.hpp"
#include "CVUtils.h"
#include "CinderArUco.h"
using namespace ci;
using namespace ci::app;
using namespace std;


/*
	A marker detection example using a library called ArUco.
	http://www.uco.es/investiga/grupos/ava/node/26

	Note that OpenCV also has it's own contributed library(also called ArCuo :p ) for doing the same thing. 
	It's currently unclear how much more work is involved to detect things compared to this library. 
	https://docs.opencv.org/3.1.0/d5/dae/tutorial_aruco_detection.html

	Note that Orbbec camera might still need drivers and can't seem to act as a WebCam with the Capture class. It currently
	doesn't even show up in found device list.

	Note this requires a x64 build of OpenCV from source due to 
	1. ArUco needs OpenCV source when building
	2. Apparently you can't build a x86 lib on a x64 machine for some reason. Not sure if it's a CMake, Visual Studio or machine issue. 

*/
class ArUcoApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;


	//! Surface object to use for non-capture mode
	SurfaceRef mDebugSurf;

	//! Capture object for capturing from webcam
	ci::CaptureRef mCapture;

	//! Textures used to render debug image or camera image
	ci::gl::TextureRef mTex,mCaptureTex,foundImage;

	//! Whether or not to use the camera or not.
	bool usecapture = true;

	CinderArUco tracker;
};


void ArUcoApp::setup()
{

	vector<ci::Capture::DeviceRef> devices = Capture::getDevices();
	CI_LOG_I("num devices " << devices.size());
	for (Capture::DeviceRef dev : devices) {
		CI_LOG_I("device name is - " << dev->getName());
	}



	//mCapture = Capture::create(app::getWindowWidth(), app::getWindowHeight(),devices[1]);
	mCapture = Capture::create(app::getWindowWidth(), app::getWindowHeight());
	mCaptureTex = gl::Texture::create(app::getWindowWidth(), app::getWindowHeight());
}

void ArUcoApp::mouseDown( MouseEvent event )
{
}

void ArUcoApp::update()
{
	
	if (usecapture) {
		if (mCapture->checkNewFrame()) {
		
			//mMarkerDetector.detect(toOcv(*mCapture->getSurface()), mMarkers);
			tracker.update(*mCapture->getSurface());
		
			mCaptureTex->update(*mCapture->getSurface());
		}
	}

}




void ArUcoApp::draw()
{

	gl::clear(Color(0, 0, 0));

	gl::draw(mCaptureTex, Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));

	tracker.draw();

}

CINDER_APP( ArUcoApp, RendererGl,[](App::Settings *settings) {
	settings->setWindowSize(1280, 720);
	settings->setMultiTouchEnabled(false);
});
